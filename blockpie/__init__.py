# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name": "Blockout Pie Menu",
    "description": "Pie Menu for Blockout and Prototyping",
    "author": "Roderick Constance",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "warning": "",
    "support": "TESTING",
    "category": "User Interface"
}


if "bpy" in locals():
    import importlib
    importlib.reload(menu)  # noqa: F821
    importlib.reload(operator)  # noqa: F821
else:
    import bpy
    from blockpie import menu
    from blockpie import operator


classes = (
    menu.CursorTo,
    menu.OriginTo,
    menu.PivotPoint,
    menu.SelectMode,
    menu.SelectType,
    menu.SelectedTo,
    menu.VIEW3D_MT_PIE_template,
    operator.AlignObjs,
    operator.CenterViewCursor,
    operator.CursorToActive,
    operator.CursorToGrid,
    operator.CursorToSelected,
    operator.OriginToCursor,
    operator.OriginToGeom,
    operator.PivotPointActiveElement,
    operator.PivotPointBoundingBoxCenter,
    operator.PivotPointCursor,
    operator.PivotPointIndivOrigin,
    operator.PivotPointMedianPoint,
    operator.SelectAll,
    operator.SelectInvert,
    operator.SelectModeEdge,
    operator.SelectModeFace,
    operator.SelectModeVertex,
    operator.SelectModeVEF,
    operator.SelectTypeCamera,
    operator.SelectTypeEmpty,
    operator.SelectTypeLight,
    operator.SelectTypeMesh,
    operator.SelectedToActive,
    operator.SelectedToCursor,
    operator.SelectedToGrid,
    operator.ViewMaterial,
    operator.ViewSolid,
    operator.ViewWireframe,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
    bpy.ops.wm.call_menu_pie(name="VIEW3D_MT_PIE_template")
