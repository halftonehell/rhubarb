# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Adatped from:
# https://blenderartists.org/t/environment-variable-for-blender-addons/1448160

import addon_utils
import os


ENVAR_BLENDER_ADDON_PATHS = "BLENDER_ADDON_PATHS"
ORIGINAL_PATHS_FUNC = addon_utils.paths


def paths():
    addon_paths = ORIGINAL_PATHS_FUNC()
    blender_addon_paths = os.environ.get(ENVAR_BLENDER_ADDON_PATHS)
    if blender_addon_paths:
        blender_addon_paths = blender_addon_paths.split(os.pathsep)
        addon_paths += [os.path.normpath(i) for i in blender_addon_paths if os.path.isdir(i)]

    return addon_paths


def register():
    addon_utils.paths = paths


def unregister():
    addon_utils.paths = ORIGINAL_PATHS_FUNC


if __name__ == "__main__":
    register()
