# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bpy
from bpy.types import Operator


class AlignObjs(Operator):
    """Align Objects"""
    bl_idname = "align.objs"
    bl_label = "AlignObjs"

    def execute(self, context):
        bpy.ops.object.align()
        return {'FINISHED'}


class CenterViewCursor(Operator):
    """Center View on Cursor"""
    bl_idname = "center_view.cursor"
    bl_label = "CenterViewCursor"

    def execute(self, context):
        bpy.ops.view3d.view_center_cursor()
        return {'FINISHED'}


class CursorToActive(Operator):
    """Cursor to Active"""
    bl_idname = "cursor_to.active"
    bl_label = "CursorToActive"

    def execute(self, context):
        bpy.ops.view3d.snap_cursor_to_active()
        return {'FINISHED'}


class CursorToGrid(Operator):
    """Cursor to Grid"""
    bl_idname = "cursor_to.grid"
    bl_label = "CursorToGrid"

    def execute(self, context):
        bpy.ops.view3d.snap_cursor_to_grid()
        return {'FINISHED'}


class CursorToSelected(Operator):
    """Cursor to Selected"""
    bl_idname = "cursor_to.selected"
    bl_label = "CursorToSelected"

    def execute(self, context):
        bpy.ops.view3d.snap_cursor_to_selected()
        return {'FINISHED'}


class OriginToCursor(Operator):
    """Origin to Cursor"""
    bl_idname = "origin_to.cursor"
    bl_label = "OriginToCursor"

    def execute(self, context):
        if context.mode == 'EDIT_MESH':
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.pivot2cursor_edit()
        elif context.mode == 'OBJECT':
            bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
        return {'FINISHED'}


class OriginToGeom(Operator):
    """Origin to Geometry"""
    bl_idname = "origin_to.geom"
    bl_label = "OriginToGeom"

    def execute(self, context):
        if context.mode == 'EDIT_MESH':
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.origintogeometry_edit()
        elif context.mode == 'OBJECT':
            bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
        return {'FINISHED'}


class PivotPointActiveElement(Operator):
    """Pivot Point Active Element"""
    bl_idname = "pivot_point.activeelement"
    bl_label = "PivotPointActiveElement"

    def execute(self, context):
        bpy.context.scene.tool_settings.transform_pivot_point = 'ACTIVE_ELEMENT'
        return {'FINISHED'}


class PivotPointBoundingBoxCenter(Operator):
    """Pivot Point Bounding Box Center"""
    bl_idname = "pivot_point.boundingboxcenter"
    bl_label = "PivotPointBoundingBoxCenter"

    def execute(self, context):
        bpy.context.scene.tool_settings.transform_pivot_point = 'BOUNDING_BOX_CENTER'
        return {'FINISHED'}


class PivotPointCursor(Operator):
    """Pivot Point 3DCursor"""
    bl_idname = "pivot_point.cursor"
    bl_label = "PivotPiontCursor"

    def execute(self, context):
        bpy.context.scene.tool_settings.transform_pivot_point = 'CURSOR'
        return {'FINISHED'}


class PivotPointIndivOrigin(Operator):
    """Pivot Point Individual Origin"""
    bl_idname = "pivot_point.indivorigin"
    bl_label = "PivotPointIndivOrigin"

    def execute(self, context):
        bpy.context.scene.tool_settings.transform_pivot_point = 'INDIVIDUAL_ORIGINS'
        return {'FINISHED'}


class PivotPointMedianPoint(Operator):
    """Pivot Point Median Point"""
    bl_idname = "pivot_point.medianpoint"
    bl_label = "PivotPointMedianPoint"

    def execute(self, context):
        bpy.context.scene.tool_settings.transform_pivot_point = 'MEDIAN_POINT'
        return {'FINISHED'}


class SelectAll(Operator):
    """Select All Toggle"""
    bl_idname = "select.all"
    bl_label = "SelectAll"

    def execute(self, context):
        if context.mode == 'EDIT_MESH':
            bpy.ops.mesh.select_all(action='TOGGLE')
        elif context.mode == 'OBJECT':
            bpy.ops.object.select_all(action='TOGGLE')
        return {'FINISHED'}


class SelectInvert(Operator):
    """Invert Selection"""
    bl_idname = "select.invert"
    bl_label = "SelectInvert"

    def execute(self, context):
        if context.mode == 'EDIT_MESH':
            bpy.ops.mesh.select_all(action='INVERT')
        elif context.mode == 'OBJECT':
            bpy.ops.object.select_all(action='INVERT')
        return {'FINISHED'}


class SelectModeEdge(Operator):
    """Select Mode Edge"""
    bl_idname = "select_mode.edge"
    bl_label = "SelectModeEdge"

    def execute(self, context):
        bpy.context.tool_settings.mesh_select_mode = (False, True, False)
        return {'FINISHED'}


class SelectModeFace(Operator):
    """Select Mode Face"""
    bl_idname = "select_mode.face"
    bl_label = "SelectModeFace"

    def execute(self, context):
        bpy.context.tool_settings.mesh_select_mode = (False, False, True)
        return {'FINISHED'}


class SelectModeVertex(Operator):
    """Select Mode Vertex"""
    bl_idname = "select_mode.vertex"
    bl_label = "SelectModeVertex"

    def execute(self, context):
        bpy.context.tool_settings.mesh_select_mode = (True, False, False)
        return {'FINISHED'}


class SelectModeVEF(Operator):
    """Select Mode Vertex/Edge/Face"""
    bl_idname = "select_mode.vef"
    bl_label = "SelectModeVEF"

    def execute(self, context):
        bpy.context.tool_settings.mesh_select_mode = (True, True, True)
        return {'FINISHED'}


class SelectedToActive(Operator):
    """Selected to Active"""
    bl_idname = "selected_to.active"
    bl_label = "SelectedToActive"

    def execute(self, context):
        bpy.ops.view3d.snap_selected_to_active()
        return {'FINISHED'}


class SelectedToCursor(Operator):
    """Selected to Cursor"""
    bl_idname = "selected_to.cursor"
    bl_label = "SelectedToCursor"

    def execute(self, context):
        bpy.ops.view3d.snap_selected_to_cursor()
        return {'FINISHED'}


class SelectedToGrid(Operator):
    """Selected to Grid"""
    bl_idname = "selected_to.grid"
    bl_label = "SelectedToGrid"

    def execute(self, context):
        bpy.ops.view3d.snap_selected_to_grid()
        return {'FINISHED'}


class SelectTypeCamera(Operator):
    """Select Type Camera"""
    bl_idname = "select_type.camera"
    bl_label = "SelectModeCamera"

    def execute(self, context):
        bpy.ops.object.select_by_type(type='CAMERA')
        return {'FINISHED'}


class SelectTypeEmpty(Operator):
    """Select Type Empty"""
    bl_idname = "select_type.empty"
    bl_label = "SelectModeEmpty"

    def execute(self, context):
        bpy.ops.object.select_by_type(type='EMPTY')
        return {'FINISHED'}


class SelectTypeLight(Operator):
    """Select Type Light"""
    bl_idname = "select_type.light"
    bl_label = "SelectModeLight"

    def execute(self, context):
        bpy.ops.object.select_by_type(type='LIGHT')
        return {'FINISHED'}


class SelectTypeMesh(Operator):
    """Select Type Mesh"""
    bl_idname = "select_type.mesh"
    bl_label = "SelectTypeMesh"

    def execute(self, context):
        bpy.ops.object.select_by_type(type='MESH')
        return {'FINISHED'}


class ViewWireframe(Operator):
    """View Wireframe"""
    bl_idname = "view.wireframe"
    bl_label = "ViewWireframe"

    def execute(self, context):
        bpy.context.space_data.shading.type = 'WIREFRAME'
        return {'FINISHED'}


class ViewSolid(Operator):
    """View Solid"""
    bl_idname = "view.solid"
    bl_label = "ViewSolid"

    def execute(self, context):
        bpy.context.space_data.shading.type = 'SOLID'
        return {'FINISHED'}


class ViewMaterial(Operator):
    """View Material"""
    bl_idname = "view.material"
    bl_label = "ViewMaterial"

    def execute(self, context):
        bpy.context.space_data.shading.type = 'MATERIAL'
        return {'FINISHED'}
