# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpy.types import Menu

import blockpie


class OriginTo(Menu):
    """Set Origin To"""
    bl_label = "Origin To"
    bl_idname = "origin.to"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Origin To")
        layout.separator()
        layout.operator(
            blockpie.operator.OriginToCursor.bl_idname,
            text="3D Cursor",
            icon='PIVOT_CURSOR'
        )
        layout.operator(
            blockpie.operator.OriginToGeom.bl_idname,
            text="Geometry",
            icon='MESH_CUBE'
        )


class PivotPoint(Menu):
    """Set Pivot Point"""
    bl_label = "Pivot Point"
    bl_idname = "pivot.point"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Pivot Point")
        layout.separator()
        layout.operator(
            blockpie.operator.PivotPointCursor.bl_idname,
            text="3D Cursor",
            icon='PIVOT_CURSOR'
        )
        layout.operator(
            blockpie.operator.PivotPointIndivOrigin.bl_idname,
            text="Individual Origin",
            icon='PIVOT_INDIVIDUAL'
        )
        layout.operator(
            blockpie.operator.PivotPointActiveElement.bl_idname,
            text="Active Element",
            icon='PIVOT_ACTIVE'
        )
        layout.operator(
            blockpie.operator.PivotPointMedianPoint.bl_idname,
            text="Median Point",
            icon='PIVOT_MEDIAN'
        )
        layout.operator(
            blockpie.operator.PivotPointBoundingBoxCenter.bl_idname,
            text="Bounding Box Center",
            icon='PIVOT_BOUNDBOX'
        )


class SelectedTo(Menu):
    """Snap Selected"""
    bl_label = "Selected To"
    bl_idname = "selected.to"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Snap Selected To")
        layout.separator()
        layout.operator(
            blockpie.operator.SelectedToGrid.bl_idname,
            text="Grid",
            icon='MESH_GRID'
        )
        layout.operator(
            blockpie.operator.SelectedToCursor.bl_idname,
            text="Cursor",
            icon='CURSOR'
        )
        layout.operator(
            blockpie.operator.SelectedToActive.bl_idname,
            text="Active",
            icon='SORTBYEXT'
        )


class CursorTo(Menu):
    """Snap Cursor"""
    bl_label = "Cursor To"
    bl_idname = "cursor.to"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Snap Cursor To")
        layout.separator()
        layout.operator(
            blockpie.operator.CursorToGrid.bl_idname,
            text="Grid",
            icon='MESH_GRID'
        )
        layout.operator(
            blockpie.operator.CursorToSelected.bl_idname,
            text="Selected",
            icon='SELECT_SET'
        )
        layout.operator(
            blockpie.operator.CursorToActive.bl_idname,
            text="Active",
            icon='SORTBYEXT'
        )


class SelectMode(Menu):
    """Set Selection Mode"""
    bl_label = "Select Mode"
    bl_idname = "select.mode"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Selection Mode")
        layout.separator()
        layout.operator(
            blockpie.operator.SelectModeVertex.bl_idname,
            text="Vertex",
            icon='VERTEXSEL'
        )
        layout.operator(
            blockpie.operator.SelectModeEdge.bl_idname,
            text="Edge",
            icon='EDGESEL'
        )
        layout.operator(
            blockpie.operator.SelectModeFace.bl_idname,
            text="Face",
            icon='FACESEL'
        )
        layout.operator(
            blockpie.operator.SelectModeVEF.bl_idname,
            text="Vertex/Edges/Faces",
            icon='OBJECT_DATA'
        )


class SelectType(Menu):
    """Set Select All Type"""
    bl_label = "Select Type"
    bl_idname = "select.type"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Selection Type")
        layout.separator()
        layout.operator(
            blockpie.operator.SelectTypeMesh.bl_idname,
            text="Mesh",
            icon='MESH_CUBE'
        )
        layout.operator(
            blockpie.operator.SelectTypeEmpty.bl_idname,
            text="Empty",
            icon='OUTLINER_DATA_EMPTY'
        )
        layout.operator(
            blockpie.operator.SelectTypeLight.bl_idname,
            text="Light",
            icon='LIGHT_DATA'
        )
        layout.operator(
            blockpie.operator.SelectTypeCamera.bl_idname,
            text="Camera",
            icon='CAMERA_DATA'
        )


class VIEW3D_MT_PIE_template(Menu):
    """Blockout Pie Menu"""
    bl_label = "Blockout Pie Menu"
    bl_idname = "blockout.pie"

    def draw(self, context):
        layout = self.layout
        pie = layout.menu_pie()
        # WEST
        pie.operator(
            blockpie.operator.ViewWireframe.bl_idname,
            text="View Wireframe",
            icon='SHADING_WIRE'
        )
        # EAST
        pie.operator(
            blockpie.operator.ViewMaterial.bl_idname,
            text="View Material",
            icon='SHADING_TEXTURE'
        )
        # SOUTH
        if context.mode == 'EDIT_MESH':
            pie.menu(
                SelectMode.bl_idname,
                text="Selection Mode",
                icon='VERTEXSEL'
            )
        elif context.mode == 'OBJECT':
            pie.menu(
                SelectType.bl_idname,
                text="Selection Type",
                icon='SELECT_SET'
            )
        # NORTH
        if context.mode == 'EDIT_MESH':
            pie.operator(
                blockpie.operator.CenterViewCursor.bl_idname,
                text="Center View Cursor",
                icon='TRACKER'
            )
        elif context.mode == 'OBJECT':
            pie.operator(
                blockpie.operator.AlignObjs.bl_idname,
                text="Align Objects",
                icon='MOD_ARRAY'
            )
        # NORTHWEST
        pie.menu(OriginTo.bl_idname, text="Origin To")
        # NORTHEAST
        pie.menu(PivotPoint.bl_idname, text="Pivot Point")
        # SOUTHWEST
        if context.mode == 'EDIT_MESH':
            pie.operator(
                blockpie.operator.SelectAll.bl_idname,
                text="Select All Toggle"
            )
        elif context.mode == 'OBJECT':
            pie.menu(
                SelectedTo.bl_idname,
                text="Snap Selected To"
            )
        # SOUTHEAST
        if context.mode == 'EDIT_MESH':
            pie.operator(
                blockpie.operator.SelectInvert.bl_idname,
                text="Invert Selection"
            )
        elif context.mode == 'OBJECT':
            pie.menu(
                CursorTo.bl_idname,
                text="Snap Cursor To"
            )
