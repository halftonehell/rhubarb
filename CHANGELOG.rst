Changelog
=========

0.1.0 (2023-05-15)
------------------
- ISSUE-009: Fixed sub-header format, Added user_startup.py. (f83fa19) [Roderick Constance]
- ISSUE-009: Fixed image formatting. (fc738ea) [Roderick Constance]
- ISSUE-009: Added README, screenshots. (2ec915d) [Roderick Constance]
- ISSUE-008: Fix blockout menu rename, whitespace after GPL header. (92f431f) [Roderick Constance]
- ISSUE-008: Added GPL headers, copyright. (50ab3bd) [Roderick Constance]
- ISSUE-007: Fixed center menu name. (4b34154) [Roderick Constance]
- ISSUE-006: Removed dynamic bl_info. (1692caf) [Roderick Constance]
- ISSUE-006: Fixed typo for unregister. (0192eb2) [Roderick Constance]
- ISSUE-006: Add pkg_resources versioning import for bl_info. (f9f76cb) [Roderick Constance]
- ISSUE-003: Changed imports back to menu, operator. (5532873) [Roderick Constance]
- ISSUE-003: Fixed import names, spacing. (a8a9a52) [Roderick Constance]
- ISSUE-005: Replaced ViewSolid with ViewMaterial operator. (98de920) [Roderick Constance]
- ISSUE-004: Changed rhubarb to blockpie. (2b29723) [Roderick Constance]
- ISSUE-003: Updated setup.py for packagename. (5815ce6) [Roderick Constance]
- ISSUE-003: Added operator import to menu. (41a4637) [Roderick Constance]
- ISSUE-003: Refactored for pip packaging. (6a1875f) [Roderick Constance]
- Dev: Added CursorTo SelectedTo snap options. (7f146bc) [Roderick Constance]
- ISSUE-001: Renamed to add-on to Block-In, removed notes. (146cbdc) [Roderick Constance]
- ISSUE-001: Fixed misc Blender syntax issues. (6210e1e) [Roderick Constance]
- ISSUE-001: Added bl_idname for rapid proto pie menu. (9c6515b) [Roderick Constance]
- ISSUE-001: Added Blender rapid protyping pie menu. (b3c28bb) [Roderick Constance]
- Initial commit. (14e16f6) [JustAddRobots]
