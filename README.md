# blockpie

Blender Pie Menu for Blockout and Prototyping

### Object-mode
<div align="center">
![Add-on Screenshot, Object-Mode](readme/screenshot-blockpie-object-default-01.png)
</div>

### Edit-mode
<div align="center">
![Add-on Screenshot, Edit-Mode](readme/screenshot-blockpie-edit-default-01.png)
</div>


## About

This Blender Pie menu provides quick access to common tools used for Blockout and prototyping.

### Object-mode
<div align="center">
![Add-on Screenshot, Object-Mode](readme/screenshot-blockpie-object-mouseover-01.png)
</div>

### Edit-mode
<div align="center">
![Add-on Screenshot, Edit-Mode](readme/screenshot-blockpie-edit-mouseover-01.png)
</div>

## Requirements

- [Python 3.6+](https://www.python.org)
- [Blender 2.8.0+](https://www.blender.org)

## Installation

This is a multifile add-on package intended for institutional deployment (works locally, too), use **pip** instead of _Blender Preferences_ for installation.

```
❯ python3 -m pip install git+https://gitlab.com/halftonehell/blockpie.git
❯ python3 -m pip show blockpie
```

- Copy *user_startup.py* to *BLENDER_USER_SCRIPTS/startup/* directory (e.g. ~/Library/Application Support/Blender/X.Y/scripts/startup/).
- Add add-on installation location to *BLENDER_ADDON_PATHS* environment variable.
- Restart Blender.
- Enable the add-on [by the usual method in Blender](https://docs.blender.org/manual/en/latest/editors/preferences/add-ons.html#enabling-disabling-add-ons).

To uninstall, disable the add-on in Blender then remove with pip.

```
❯ python3 -m pip uninstall blockpie
```

## Support

This proof-of-concept is an alpha, thus currently unsupported.

## License

Licensed under GNU GPL v3. See [LICENSE](LICENSE).
